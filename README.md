# This repository is obsolete #
Use this one instead: https://bitbucket.org/europeanspallationsource/opticstests

.
.
.
.
.
.
.
.
.
.
.
.
.
.
.

This is a repository for my changes to the EPICS areaDetector module version 1.9.1

2014-04-18 15:44:02
Adding EMGAIN modifications.
Untested on the real hardware.

2014-04-25 20:17:55 
Modifications to the EMGAIN code.
Tested on the real hardware.

2014-05-12 11:46:15
Minor Andor fixes.

2014-05-12 11:53:15
Build Simulator on MAC OS X.

2014-05-12 11:53:35
Prosilica lens control, build on MAC OS X.
Lens control is from ESS FSIA module.

2014-05-12 11:56:25
NDStats changes - adding Moments calculations (adopt ImageJ code) from
http://rsb.info.nih.gov/ij/plugins/download/Moment_Calculator.java

2014-05-12 11:59:11
Do not use ImageMagick and update RELEASE.

2014-05-12 12:07:11
Add Prosilica len control to the OPI.
Lens control OPI is from ESS FSIA module.

2014-05-12 12:07:11
Add delta and epsilon parameters to the simulated peaks to allow skew and
kurtosis manipulation.

2014-05-22 19:45:36
Update Andor libs for x86_64.

2014-05-22 19:48:08
Add OPI for showing image from ArrayData.

2014-07-14 14:52:43 
Andor Luca changes - make cooler/set temperature/shutter calls depend on the
camera capablities.
Add capabilities to the object.
Cleanly exit by waiting for the status and data threads to shutdown.

2015-01-26 12:09:29 
Support for Thorlabs CCS spectrometers - USB kind.