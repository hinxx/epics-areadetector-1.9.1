/* NDFileAscii.cpp
 * ASCII file writer, whose main purpose is to create textual representation of
 * the data arriving from Thorlabs CCS.
 *
 * Hinko Kocevar
 * March 18, 2015
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <epicsStdio.h>
#include <iocsh.h>

#include <epicsExport.h>

#include "NDFileAscii.h"

static const char *driverName = "NDFileAscii";

#define MAX_ATTRIBUTE_STRING_SIZE 256

/** Opens a ASCII file.
  * \param[in] fileName The name of the file to open.
  * \param[in] openMode Mask defining how the file should be opened; bits are 
  *            NDFileModeRead, NDFileModeWrite, NDFileModeAppend, NDFileModeMultiple
  * \param[in] pArray A pointer to an NDArray; this is used to determine the array and attribute properties.
  */
asynStatus NDFileAscii::openFile(const char *fileName, NDFileOpenMode_t openMode, NDArray *pArray)
{
    NDAttribute *attr;

    //printf("%s: ENTER\n", __func__);
    //printf("%s: fileName %s\n", __func__, fileName);

    attr = pArray->pAttributeList->find("DeviceName");
    if (! attr) {
		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n",
				driverName, __func__, "This is not a ThoLabs CCSxxx data!");
	    return(asynError);
    }

    /* Create the file. */
	if ((this->outFile = fopen(fileName, "w")) == NULL) {
		asynPrint(this->pasynUserSelf, ASYN_TRACE_ERROR,
		"%s:%s error opening file %s\n",
		driverName, __func__, fileName);
		return(asynError);
	}

    //printf("%s: LEAVE SUCCESS\n", __func__);
    return(asynSuccess);
}

/** Writes single NDArray to the ASCII file.
  * \param[in] pArray Pointer to the NDArray to be written
  */
asynStatus NDFileAscii::writeFile(NDArray *pArray)
{
    NDArrayInfo info;
	char buf[256];
	int ret;
	size_t i, xDim;
	struct tm *tm;
	time_t t;
    NDAttribute *attrDeviceName;
    NDAttribute *attrSerialNumber;
    NDAttribute *attrFirmwareRevision;
    NDAttribute *attrProcessedData;
    char deviceName[256];
    char serialNumber[256];
    char firmwareRevision[256];
    int processedData;

    //printf("%s: ENTER\n", __func__);

    if (! outFile) {
		asynPrint(this->pasynUserSelf, ASYN_TRACE_ERROR,
		"%s:%s file not opened\n",
		driverName, __func__);
		return(asynError);
    }
    //printf("%s: n dimensions %d\n", __func__, pArray->ndims);

    pArray->getInfo(&info);
    //printf("%s: n Elements %d\n", __func__, info.nElements);
    //printf("%s: total bytes %d\n", __func__, info.totalBytes);

    //printf("%s: dimension X size %d\n", __func__, pArray->dims[0].size);
    //printf("%s: dimension Y size %d\n", __func__, pArray->dims[1].size);
    //printf("%s: dimension Z size %d\n", __func__, pArray->dims[2].size);
	epicsFloat64 *pData = (epicsFloat64 *)pArray->pData;
	epicsFloat64 *pWData = (epicsFloat64 *)pArray->pData + pArray->dims[0].size;
	epicsFloat64 *pAData = (epicsFloat64 *)pArray->pData + 2 * pArray->dims[0].size;
	xDim = pArray->dims[0].size;
    attrDeviceName = pArray->pAttributeList->find("DeviceName");
    attrDeviceName->getValue(NDAttrString, deviceName, 0);
    attrSerialNumber = pArray->pAttributeList->find("SerialNumber");
    attrSerialNumber->getValue(NDAttrString, serialNumber, 0);
    attrFirmwareRevision = pArray->pAttributeList->find("FirmwareRevision");
    attrFirmwareRevision->getValue(NDAttrString, firmwareRevision, 0);
    attrProcessedData = pArray->pAttributeList->find("ProcessedData");
    attrProcessedData->getValue(NDAttrInt32, &processedData, 0);
    //printf("%s: processed Data %d\n", __func__, processedData);

    t = time(NULL);
    tm = localtime(&t);
	strftime(buf, sizeof(buf), "# Saved on: %a %d %b %Y %H:%M:%S %z\n", tm);
	ret = fwrite(buf, 1, strlen(buf), outFile);
	if (ret < 0) {
		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n", driverName,
				__func__, strerror(errno));
		return(asynError);
	}

	ret = fprintf(outFile, "# Device information: Thorlabs %s, serial %s, firmware %s\n",
			deviceName, serialNumber, firmwareRevision);
	if (ret < 0) {
		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n", driverName,
				__func__, strerror(errno));
		return(asynError);
	}

	if (processedData) {
		ret = fprintf(outFile, "# data\t\twavelength\tamplitude\n");
		if (ret < 0) {
			asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n", driverName,
					__func__, strerror(errno));
			return(asynError);
		}

		// data size should be TLCCS_NUM_PIXELS
		for (i = 0; i < xDim; i++) {
			ret = fprintf(outFile, "%6.6f\t%6.6f\t%6.6f\n",
					*(pData + i), *(pWData  + i), *(pAData + i));
			if (ret < 0) {
				asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n", driverName,
						__func__, strerror(errno));
				return(asynError);
			}
		}
	} else {
		ret = fprintf(outFile, "# raw\t\twavelength\tamplitude\n");
		if (ret < 0) {
			asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n", driverName,
					__func__, strerror(errno));
			return(asynError);
		}

		// dataSize should be TLCCS_NUM_RAW_PIXELS
		for (i = 0; i < xDim; i++) {
			// see tlccs.c tlccs_aquireRawScanData()
			ret = fprintf(outFile, "%6.6d\t%6.6f\t%6.6f\n",
					(epicsInt32)(*(pData + i)), *(pWData  + i), *(pAData + i));
			if (ret < 0) {
				asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n", driverName,
						__func__, strerror(errno));
				return(asynError);
			}
		}
	}

    //printf("%s: LEAVE SUCCESS\n", __func__);
    return(asynSuccess);
}

/** Reads single NDArray from a ASCII file; NOT CURRENTLY IMPLEMENTED.
  * \param[in] pArray Pointer to the NDArray to be read
  */
asynStatus NDFileAscii::readFile(NDArray **pArray)
{
    return asynError;
}


/** Closes the ASCII file. */
asynStatus NDFileAscii::closeFile()
{
    //printf("%s: ENTER\n", __func__);
    fclose(outFile);
    //printf("%s: LEAVE SUCCESS\n", __func__);
    return(asynSuccess);
}


/** Constructor for NDFileAscii; all parameters are simply passed to NDPluginFile::NDPluginFile.
  * \param[in] portName The name of the asyn port driver to be created.
  * \param[in] queueSize The number of NDArrays that the input queue for this plugin can hold when 
  *            NDPluginDriverBlockingCallbacks=0.  Larger queues can decrease the number of dropped arrays,
  *            at the expense of more NDArray buffers being allocated from the underlying driver's NDArrayPool.
  * \param[in] blockingCallbacks Initial setting for the NDPluginDriverBlockingCallbacks flag.
  *            0=callbacks are queued and executed by the callback thread; 1 callbacks execute in the thread
  *            of the driver doing the callbacks.
  * \param[in] NDArrayPort Name of asyn port driver for initial source of NDArray callbacks.
  * \param[in] NDArrayAddr asyn port driver address for initial source of NDArray callbacks.
  * \param[in] priority The thread priority for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  * \param[in] stackSize The stack size for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  */
NDFileAscii::NDFileAscii(const char *portName, int queueSize, int blockingCallbacks,
                       const char *NDArrayPort, int NDArrayAddr,
                       int priority, int stackSize)
    /* Invoke the base class constructor.
     * We allocate 2 NDArrays of unlimited size in the NDArray pool.
     * This driver can block (because writing a file can be slow), and it is not multi-device.  
     * Set autoconnect to 1.  priority and stacksize can be 0, which will use defaults. */
    : NDPluginFile(portName, queueSize, blockingCallbacks,
                   NDArrayPort, NDArrayAddr, 1, NUM_NDFILE_ASCII_PARAMS,
                   2, 0, asynGenericPointerMask, asynGenericPointerMask, 
                   ASYN_CANBLOCK, 1, priority, stackSize),
      outFile(NULL)
{
    //const char *functionName = "NDFileAscii";

    /* Set the plugin type string */    
    setStringParam(NDPluginDriverPluginType, "NDFileAscii");
    this->supportsMultipleArrays = 0;

    //printf("%s: ENTER\n", __func__);
}

/* Configuration routine.  Called directly, or from the iocsh  */

extern "C" int NDFileAsciiConfigure(const char *portName, int queueSize, int blockingCallbacks,
                                   const char *NDArrayPort, int NDArrayAddr,
                                   int priority, int stackSize)
{
    new NDFileAscii(portName, queueSize, blockingCallbacks, NDArrayPort, NDArrayAddr,
                   priority, stackSize);
    return(asynSuccess);
}


/* EPICS iocsh shell commands */

static const iocshArg initArg0 = { "portName",iocshArgString};
static const iocshArg initArg1 = { "frame queue size",iocshArgInt};
static const iocshArg initArg2 = { "blocking callbacks",iocshArgInt};
static const iocshArg initArg3 = { "NDArray Port",iocshArgString};
static const iocshArg initArg4 = { "NDArray Addr",iocshArgInt};
static const iocshArg initArg5 = { "priority",iocshArgInt};
static const iocshArg initArg6 = { "stack size",iocshArgInt};
static const iocshArg * const initArgs[] = {&initArg0,
                                            &initArg1,
                                            &initArg2,
                                            &initArg3,
                                            &initArg4,
                                            &initArg5,
                                            &initArg6};
static const iocshFuncDef initFuncDef = {"NDFileAsciiConfigure",7,initArgs};
static void initCallFunc(const iocshArgBuf *args)
{
    NDFileAsciiConfigure(args[0].sval, args[1].ival, args[2].ival, args[3].sval, args[4].ival, args[5].ival, args[6].ival);
}

extern "C" void NDFileAsciiRegister(void)
{
    iocshRegister(&initFuncDef,initCallFunc);
}

extern "C" {
epicsExportRegistrar(NDFileAsciiRegister);
}
