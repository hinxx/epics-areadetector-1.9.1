< envPaths

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "10000000")

epicsEnvSet("STREAM_PROTOCOL_PATH", "$(TOP)/db")

errlogInit(20000)

dbLoadDatabase("$(AREA_DETECTOR)/dbd/tlCCSApp.dbd")
tlCCSApp_registerRecordDeviceDriver(pdbbase) 

< ccs100.cmd

< ccs200.cmd

#< pm100usb.cmd

iocInit()

< ccs100_autosave.cmd
< ccs200_autosave.cmd
